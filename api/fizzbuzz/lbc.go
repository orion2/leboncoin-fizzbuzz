package lbc

import (
    "fmt"
    "net/http"
    "strconv"
    "strings"

    "github.com/labstack/echo"
	"github.com/go-redis/redis"

    "./db"
)

// echo router declaration ---------------------------------------------------- |

// echo custom context
type (dbContext struct {
    echo.Context
    db *redis.Client
})

// RouterFizzBuzz ...
func RouterFizzBuzz(e *echo.Echo, r *redis.Client) {
    e.Use(func(h echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			cc := &dbContext{c, r}
			return h(cc)
		}
	})
    routerLbcFizzBuzz := e.Group("/leboncoin")
    routerLbcFizzBuzz.Match([]string{"POST"}, "/fizzbuzz", handlerFizzBuzz)
    routerLbcFizzBuzz.Match([]string{"GET"}, "/statistics", handlerStatistics)
    fmt.Println(r)
}

// fizzbuzz ------------------------------------------------------------------- |

// FizzBuzzData ...
type FizzBuzzData struct {
    Int1    int             `json:"int1"`
    Int2    int             `json:"int2"`
    Limit   int             `json:"limit"`
    Str1    string          `json:"str1"`
    Str2    string          `json:"str2"`
}

// ResponseFizzBuzz ...
type ResponseFizzBuzz struct {
    Listofstrings   []string  `json:"lstr"`
}

// handlerFizzBuzz ...
func handlerFizzBuzz(c echo.Context) error {
    // context and db ready to use
    cc := c.(*dbContext)
    fmt.Println(cc)
    // extract data from request
    idta := bindQueryParameters(cc)
    fmt.Println(idta)
    // create data for response
    lstr := buildOneToLimit(idta)
    // answer request with response
    resp := ResponseFizzBuzz{
        Listofstrings: lstr,
    }
    return c.JSON(http.StatusCreated, resp)
}

// bindQueryParameters ...
func bindQueryParameters(c *dbContext) *FizzBuzzData { 
    fizzbuzzdata := new(FizzBuzzData)
    if err := c.Bind(fizzbuzzdata); err != nil {
        fmt.Println(err)
    }
    fmt.Println("> Store")
    storeQueryParameters(fizzbuzzdata, c.db)
    fmt.Println("< Store")
    return fizzbuzzdata
}

// storeQueryParametser ...
func storeQueryParameters(fb *FizzBuzzData, r *redis.Client) {
    strint := strconv.Itoa(fb.Int1) +"."+ strconv.Itoa(fb.Int2) +"."+ strconv.Itoa(fb.Limit)
    str := strint +"."+ fb.Str1 +"."+ fb.Str2
    rds.StoreRequest(r, str)
    fmt.Println(str)
}

// buildOneToLimit ...
func buildOneToLimit(reqdata *FizzBuzzData) []string {
    lstr := []string{""}
    sum := 1
    for sum < reqdata.Limit {
        if (sum % reqdata.Int1 == 0) {
            // fmt.Println(sum, reqdata.Int1, sum % reqdata.Int1, reqdata.Str1)
            lstr = append(lstr, reqdata.Str1)
        } else if (sum % reqdata.Int2 == 0) {
            // fmt.Println(sum, reqdata.Int2, sum % reqdata.Int2, reqdata.Str2)
            lstr = append(lstr, reqdata.Str2)
        } else {
            // fmt.Println(sum)
            lstr = append(lstr, strconv.Itoa(sum))
        }
        sum++
    }
    return lstr
}

// statistics ----------------------------------------------------------------- |

// ResponseStatisticsList ...
type ResponseStatisticsList struct {
    Responses   []ResponseStatistics    `json:"responses"`
}

// ResponseStatistics ...
type ResponseStatistics struct {
    Mostused    FizzBuzzData    `json:"mostused"`
    Hits        int             `json:"hits"`
}

// handlerStatistics ...
func handlerStatistics(c echo.Context) error {
    cc := c.(*dbContext)
    max := rds.GetMaxRequest(cc.db)
    resp := rebuildFizzBuzzDataFromKeyString(max)
    return c.JSON(http.StatusCreated, resp)
}

// rebuildFizzBuzzDataFromKeyString ...
func rebuildFizzBuzzDataFromKeyString(keystringlist []string) ResponseStatisticsList {
    fmt.Println(keystringlist)
    rsp := ResponseStatisticsList{}
    for cnt ,keystring := range keystringlist {
        fmt.Println(cnt)
        slice := strings.Split(keystring, ".")
        rp := ResponseStatistics{
            Mostused: FizzBuzzData{
                Int1:   stringToInt(slice[0]),
                Int2:   stringToInt(slice[1]),
                Limit:  stringToInt(slice[2]),
                Str1:   slice[3],
                Str2:   slice[4],
            },
            Hits:    stringToInt(slice[5]),
        }
        rsp.Responses = append(rsp.Responses, rp)
    }
    return rsp
}

// stringToInt ...
func stringToInt(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(err)
    }
    return i
}
