package rds

import (
	"fmt"
	"strconv"
	// "reflect"
	// "encoding/json"

	"github.com/go-redis/redis"
)

// api ------------------------------------------------------------------------ |

// RedisNewClient ...
func RedisNewClient() *redis.Client{
	client := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	return client
}

// fizzbuzz ------------------------------------------------------------------ |

// StoreRequest ...
func StoreRequest(redcli *redis.Client, keystring string) {
	val := keyExists(redcli, keystring)
	if val == 0 {
		fmt.Println("!Key")
		insertKeyVal(redcli, keystring, 1)
	} else {
		fmt.Println("=Key")
		val++
		fmt.Println(val)
		insertKeyVal(redcli, keystring, val)
	}
}

// keyExists ...
func keyExists(redcli *redis.Client, keystring string) int {
	bol, err := redcli.HExists("lbc", keystring).Result()
	CheckErr(err)
	if bol == false {
		fmt.Println(keystring, "!exists", bol)
		return 0
	}
	fmt.Println(keystring, "exists", bol)
	return getKeyVal(redcli, keystring)
}

// GetKeyVal ...
func getKeyVal(redcli *redis.Client, keystring string) int {
	fmt.Println(keystring)
	val, err := redcli.HGet("lbc", keystring).Result()
	// fmt.Println(">>>", val, err)
	CheckErr(err)
	i, err := strconv.Atoi(val)
	return i
}

// insertKeyVal ...
func insertKeyVal(redcli *redis.Client, keystring string, val int) {
	err := redcli.HSet("lbc", keystring, val).Err()
	if err != nil {
		panic(err)
	}
}

// statistics ----------------------------------------------------------------- |

// GetMaxRequest ...
func GetMaxRequest(redcli *redis.Client) []string {
	keystringlist := []string{}
	maxkeystring := ""
	maxcnt := 1
	keys, err1 := redcli.HGetAll("lbc").Result()
	fmt.Println(keys)
	CheckErr(err1)
	for keystring, scnt := range keys {
		icnt, err2 := strconv.Atoi(scnt)
		CheckErr(err2)
		if icnt > maxcnt {
			maxkeystring = keystring
			maxcnt = icnt
			keystringlist = []string{maxkeystring + "." + strconv.Itoa(maxcnt)}
		} else if icnt == maxcnt {
			keystringlist = append(keystringlist, keystring + "." + strconv.Itoa(maxcnt))
		}
	}
	fmt.Println(">>> keystringlist =>", keystringlist)
	return keystringlist
}

// error-handling ------------------------------------------------------------- |

// CheckErr ...
func CheckErr(err error) {
	if err != nil {
		fmt.Println("CheckErr() => ", err)
		// panic(err)
	}
}
