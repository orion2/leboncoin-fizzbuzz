package main

import (
	"net/http"
	"io/ioutil"
	"encoding/json"
	"fmt"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"./fizzbuzz"
	"./fizzbuzz/db"

)

func main() {

	e := echo.New()
	
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.Secure())

	r := rds.RedisNewClient()
	defer r.Close()
	
	lbc.RouterFizzBuzz(e, r)
	data, err := json.MarshalIndent(e.Routes(), "", "  ")
	if err != nil {
		fmt.Println(err)
	}
	ioutil.WriteFile("routes.json", data, 0644)

	e.Logger.Fatal(e.Start(":10000"))
}
