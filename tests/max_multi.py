import requests

user_1 = {
  "int1":4,
  "int2":7,
  "limit":100,
  "str1":"yo",
  "str2":"dam"
}

user_2 = {
  "int1":4,
  "int2":7,
  "limit":100,
  "str1":"yo",
  "str2":"pao"
}

resp_1 = requests.post(
  url="http://localhost:10000/leboncoin/fizzbuzz",
  json=user_1
)

print(f'{resp_1.status_code}')

resp_2 = requests.post(
  url="http://localhost:10000/leboncoin/fizzbuzz",
  json=user_2
)

print(f'{resp_2.status_code}')

statistics = requests.get(url="http://localhost:10000/leboncoin/statistics",).json()

print(f'{statistics}')
