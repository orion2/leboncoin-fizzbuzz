# LeBonCoin FizzBuzz API

## Run
> docker-compose up
---
### Routes
#### POST /leboncoin/fizzbuzz
- __IN__ (json-payload)
  - Int1  : int
  - Int2  : int
  - Limit : int
  - Str1  : string
  - Str2  : string
- __OUT__ (return)
  - Lstr  : [string]
#### GET /leboncoin/statistics 
- __IN__ (json-payload)
  - None
- __OUT__ (return)
  - Responses = [{ MostUsedRequest, Hints }]
